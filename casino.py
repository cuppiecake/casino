import random
import sys


wallet
# Global Variables


def start():
  print("Welcome to the Casino!")
  wallet = 0
  instructions()



def instructions():
  print("Enter R to roll")
  print("Enter Q to quit")
  print("Enter W to check your wallet")
  print("Enter A to add cash")
  while True:
    ans = str(input().lower())
    if 'r' in ans:
      if wallet > 0:
        roll()
        return
      else:
        print("Please insert money in your wallet to play!")
        continue
    elif 'q' in ans:
      end()
      return
    elif 'w' in ans:
      print(f"You have ${wallet} in  your wallet.")
      continue
    elif 'a' in ans:
      print("How much money would you like to add?")
      ans2 = input()
      wallet += ans2
      print(f"You now have ${wallet} in your wallet.")
      continue
    else:
      print("Please input a valid option.")
      continue

oddsArr = [];
def createOdds(num):
  for x in range(0, (num - 1)):
    oddsArr.append(0)
  
  oddsArr.append(2)

  random.shuffle(oddsArr)
  pick('odds')


weightArr = []
def weightedOdds(winning, even, losing):
  for x in range(0, winning):
    weightArr.append(2)
  
  for x in range(0, even):
    weightArr.append(1)

  for x in range(0, losing):
    weightArr.append(0)
  
  random.shuffle(weightArr)
  pick('weight')

def delWeight():
  for x in weightArr:
    del weightArr[0]

def delOdds():
  for x in oddsArr:
    del oddsArr[0]

def pick(type):
  if str(type) == 'weight':
    results = weightArr.pop()
  elif str(type) == 'odds':
    results = oddsArr.pop()

  printResults(results, type)
  

def printResults(num, type):
  if num == 0:
    print("You lost.")
  elif num == 1:
    print("It looks like you broke even.")
  elif num == 2:
    print("You won!")
  
  print("")
  print("Play some more? (Enter Y or N)")
  while True:
    ans = str(input()).lower()
    if 'y' in ans:
      if str(type) == 'weight':
        delWeight()
        weightedOdds(2, 5, 7)
      elif str(type) == 'odds':
        delOdds()
        createOdds(100)
      
      return
    elif 'n' in ans:
      roll()
      return
    else:
      print("Please input a valid option.")
      continue   


def roll():
  print("Would you like to try your luck at the lottery, or the gambling table?")
  print("Enter L for lottery")
  print("Enter G for gambling")
  print("Enter Q to quit")
  while True:
    ans = str(input()).lower()
    if 'l' in ans:
      createOdds(100)
      return
    elif 'g' in ans:
      weightedOdds(2, 5, 7)
      return
    elif 'q' in ans:
      end()
    else:
      print("Please input a valid option.")
      continue

def end():
  print("Come again soon!")
  quit()

start()